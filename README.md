[![Codacy Badge](https://app.codacy.com/project/badge/Grade/8bc20bd1796e4495a7d5e4e409ce478e)](https://www.codacy.com/manual/Nitin1818/Logan_2?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=Nitin1818/Logan&amp;utm_campaign=Badge_Grade)

# Loganbot

A modular Telegram bot running on python3 with an sqlalchemy database, with some extra fun.


fork of haruka

## Thanks to

* MrYacha - For pYanaBot :3
* Skittle - for memes and sticker stuff
* 1mavarick1 - gmutes, etc 
* AyraHikari - weather
* Paul Larsen - marie creator, inspiration to do many things
* FFBot - for translations functions
And much more that we couldn't list it here!
